'use strict';

var app = angular.module('taxiClientApp');

app.service('BookingsSyncService', function ($rootScope, $http) {
  var lastBookingId = 0;
  return {
    save: function (bookingDetails, callback) {
      $http
        .post('http://localhost:3000/bookings', {booking: bookingDetails})
        .success(function (data) {
          lastBookingId = data.bookingId;
          callback(data.message);
        });
    },
    getLastBookingId: function() { return lastBookingId; }
  };
});
