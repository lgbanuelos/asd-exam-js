'use strict';

/**
 * @ngdoc function
 * @name taxiHomeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the taxiHomeApp
 */
angular.module('taxiHomeApp')
  .controller('MainCtrl', function ($scope, STRSService) {
    $scope.showModal = STRSService.activateModal;
    $scope.bookings = STRSService.getBookings();
  });
