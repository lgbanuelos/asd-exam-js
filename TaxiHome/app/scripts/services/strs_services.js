'use strict';

var app = angular.module('taxiHomeApp');

app.service('STRSService', function (btfModal, AsyncNotifModal, $http) {
  var bookings = [];
  var booking = {};

  console.log('Activated the service');
  var pusher = new Pusher('a5413d7514e523255c2e');
  var channel = pusher.subscribe('strs');
  channel.bind('booking_request', function (data) {
    booking = data;
    AsyncNotifModal.activate();
  });

  return {
    notifyDecision: function (decision) {
      booking.status = decision;
      $http.post('http://localhost:3000/taxiAssignments', {'booking': booking});
      bookings.splice(0, 0, booking);
      AsyncNotifModal.deactivate();
    },
    activateModal: function() { AsyncNotifModal.activate(); },
    getBooking: function() { return booking; },
    getBookings: function() { return bookings; }
  };
});
