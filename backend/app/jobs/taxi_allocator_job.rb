class TaxiAllocatorJob
  include SuckerPunch::Job

  def perform(booking_id)
    booking = Booking.find(booking_id)
    booking.taxi = nil

    select_taxi(booking)
    booking = Booking.find(booking_id)

    Pusher.trigger('strs','booking_request', booking.to_json)
    Pusher.logger.info 'SENT VIA PUSHER'
    booking.update_attributes status: 'FORWARDED'
  end

  def perform_later(sec, booking_id)
    after(sec) {
      perform(booking_id)
    }
  end

  def compute_distance(addr1, addr2)
    addr1 << ', Tartu, Estonia'
    addr2 << ', Tartu, Estonia'
    url = "http://maps.googleapis.com/maps/api/directions/json?origin=#{ CGI.escape( addr1 ) }&destination=#{ CGI.escape( addr2 ) }&sensor=false&mode=driving"

    resp = JSON.parse( RestClient.get(url) )
    resp['routes'][0]['legs'].map {|m| m['distance']['value']}.reduce(:+)
  end

  def select_taxi(booking)
    rejections = BookingRejection.where(:booking_id => booking.id).map { |rj| rj.taxi_id }

    taxis_hash = {}
    Taxi.find(:all).each do |taxi|
      unless rejections.include?(taxi.id)
        distance = compute_distance(booking.pickup_address, taxi.current_address)
        taxis_hash[distance] = taxi
      end
    end

    taxis_hash.sort.map do |distance, taxi|
      booking.taxi = taxi
      booking.save
      break
    end
  end
end
