class BookingRejection < ActiveRecord::Base
  belongs_to :taxi
  belongs_to :booking
end
